import type {NextPage, GetStaticPaths, GetStaticProps, InferGetStaticPropsType} from 'next';
import Link from 'next/link';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import type {Country} from '../../types/Country';

export const getStaticPaths: GetStaticPaths<{countryCode: string}> = async () => {
  const response = await fetch('https://restcountries.com/v3/all?fields=cca3');
  const countries: Country[] = await response.json();
  const paths = countries.map(({cca3}) => ({
    params: {
      countryCode: cca3.toLowerCase(),
    },
  }));

  return {
    paths,
    fallback: 'blocking',
  };
};

export const getStaticProps: GetStaticProps<
  {country: Country; borderCountries: Country[]},
  {countryCode: string}
> = async function getStaticProps({params}) {
  const response = await fetch(`https://restcountries.com/v3/alpha/${params?.countryCode}`);
  const country: Country = (await response.json())[0];

  let borderCountries = [];

  if (country?.borders) {
    const borderCountriesResponse = await fetch(
      `https://restcountries.com/v3/alpha?codes=${country.borders.join(',')}&fields=name,cca3`,
    );
    borderCountries = await borderCountriesResponse.json();
  }

  return {
    props: {
      country,
      borderCountries,
    },
  };
};

const CountryDetails: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({
  country,
  borderCountries,
}) => {
  return (
    <>
      <div className="mt-6 mb-20">
        <Link href="/">
          <a className="rounded-md dark:bg-darkmodeElement py-2 px-8 shadow-xl">
            <FontAwesomeIcon icon={faArrowLeft} />
            &nbsp; Back
          </a>
        </Link>
      </div>
      <div className="desktop:mt-18 desktop:flex desktop:flex-row desktop:gap-x-28">
        <div className="desktop:w-6/12">
          <img src={country.flags?.[0]} alt={country.name.common} className="w-full" />
        </div>
        <div className="desktop:w-6/12 desktop:flex desktop:items-center">
          <div className="w-full">
            <h2 className="font-extrabold text-3xl my-8">{country.name.common}</h2>
            <div className="desktop:flex desktop:flex-row">
              <div className="desktop:w-1/2">
                <div className="my-1">
                  <span className="font-semibold">Native Name: </span>
                  <span className="font-light">
                    {country.name.nativeName?.[Object.keys(country.languages || {})[0]]?.common}
                  </span>
                </div>
                <div className="my-1">
                  <span className="font-semibold">Area: </span>
                  <span className="font-light">{country.area}</span>
                </div>
                <div className="my-1">
                  <span className="font-semibold">Region: </span>
                  <span className="font-light">{country.region}</span>
                </div>
                <div className="my-1">
                  <span className="font-semibold">Sub Region: </span>
                  <span className="font-light">{country.subregion}</span>
                </div>
                <div className="my-1">
                  <span className="font-semibold">Capital: </span>
                  <span className="font-light">{country.capital?.[0]}</span>
                </div>
                <br />
              </div>
              <div className="desktop:w-1/2">
                <div className="my-1">
                  <span className="font-semibold">Top Level Domain: </span>
                  <span className="font-light">{country.tld?.[0]}</span>
                </div>
                <div className="my-1">
                  <span className="font-semibold">Currencies: </span>
                  <span className="font-light">
                    {Object.values(country.currencies || {})
                      .map(({name}) => name)
                      .join(', ')}
                  </span>
                </div>
                <div className="my-1">
                  <span className="font-semibold">Languages: </span>
                  <span className="font-light">
                    {Object.values(country.languages || {}).join(', ')}
                  </span>
                </div>
              </div>
            </div>
            <br />
            <div className="flex flex-wrap gap-2">
              <h3 className="font-semibold text-lg mobile:w-full">Border Countries:</h3>
              {borderCountries.map(({cca3, name}) => (
                <Link key={cca3} href={`/${cca3.toLowerCase()}`}>
                  <a className="py-0.5 px-6 bg-lightmodeElement dark:bg-darkmodeElement shadow-xl rounded text-center">
                    {name.common}
                  </a>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CountryDetails;
