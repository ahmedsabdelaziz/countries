import {useState, useEffect} from 'react';
import type {AppProps} from 'next/app';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faMoon as faMoonRegular} from '@fortawesome/free-regular-svg-icons';
import {faMoon as faMoonSolid} from '@fortawesome/free-solid-svg-icons';
import '../styles/global.css';

function App({Component, pageProps}: AppProps) {
  const [theme, setTheme] = useState<'light' | 'dark'>('light');
  useEffect(() => {
    document.documentElement.className = theme;
  }, [theme]);

  return (
    <>
      <header className="flex justify-center py-5 shadow  bg-lightmodeElement dark:text-darkmodeText dark:bg-darkmodeElement transition-colors">
        <div className="container flex justify-between">
          <h1 className="font-extrabold text-xl">Where in the world?</h1>
          <button
            className="font-semibold"
            onClick={() => {
              if (theme == 'light') {
                setTheme('dark');
              } else {
                setTheme('light');
              }
            }}
          >
            <FontAwesomeIcon icon={theme == 'light' ? faMoonRegular : faMoonSolid} />
            &nbsp;
            {theme == 'light' ? ' Dark Mode' : 'Light Mode'}
          </button>
        </div>
      </header>
      <main className="flex justify-center py-5 desktop:py-12">
        <div className="container">
          <Component {...pageProps} />
        </div>
      </main>
    </>
  );
}
export default App;
