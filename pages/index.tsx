import {useState, useMemo} from 'react';
import type {NextPage, GetStaticProps, InferGetStaticPropsType} from 'next';
import Head from 'next/head';
import Link from 'next/link';
import {
  ListboxInput,
  ListboxButton,
  ListboxPopover,
  ListboxList,
  ListboxOption,
} from '@reach/listbox';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronDown, faTimesCircle} from '@fortawesome/free-solid-svg-icons';
import type {Country} from '../types/Country';
import SearchBox from '../components/SearchBox';
import CountryCard from '../components/CountryCard';

export const getStaticProps: GetStaticProps<{countries: Country[]}> =
  async function getStaticProps() {
    const response = await fetch(
      'https://restcountries.com/v3/all?fields=name,cca3,flags,area,region,capital',
    );
    const countries = await response.json();

    return {
      props: {countries},
    };
  };

const Home: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({countries}) => {
  const [search, setSearch] = useState('');
  const [selectedRegion, setSelectedRegion] = useState<string | undefined>(undefined);

  const regions = useMemo(() => {
    const set = countries.reduce((accumulator, country) => {
      if (country.region) {
        accumulator.add(country.region);
      }

      return accumulator;
    }, new Set<string>());

    return Array.from(set).sort();
  }, [countries]);

  const filteredCountries = useMemo(() => {
    return countries.filter(
      ({name, region}) =>
        (!selectedRegion || region == selectedRegion) &&
        name.common.toLowerCase().includes(search.toLowerCase()),
    );
  }, [search, selectedRegion, countries]);

  return (
    <>
      <Head>
        <title>Countries</title>
      </Head>
      <div className="flex flex-col desktop:flex-row gap-8 mb-8 justify-between">
        <SearchBox
          className="w-full desktop:w-2/5 shadow"
          placeholder="Search for a country..."
          value={search}
          onSearch={setSearch}
        />
        <ListboxInput
          className={`
              w-2/3
              desktop:w-1/6
              rounded-md
              bg-lightmodeElement
              dark:bg-darkmodeElement
              shadow
              focus-within:outline-[auto]
              `}
          value={selectedRegion || 'default'}
          onChange={setSelectedRegion}
        >
          <ListboxButton
            className="w-full rounded-md border-none py-4 px-6 focus:outline-none"
            arrow={
              <FontAwesomeIcon
                size="2x"
                icon={selectedRegion ? faTimesCircle : faChevronDown}
                onClick={(event) => {
                  if (selectedRegion) {
                    setSelectedRegion(undefined);
                  }
                }}
              />
            }
          />
          <ListboxPopover
            className="
              mt-2
              py-4
              px-6
              rounded-md
              border-none
              shadow
              bg-lightmodeElement
              text-lightmodeText
              dark:bg-darkmodeElement
              dark:text-darkmodeText
            "
          >
            <ListboxList>
              <ListboxOption value="default" className="hidden">
                Filter by Region
              </ListboxOption>
              {regions.map((region) => (
                <ListboxOption key={region} value={region}>
                  {region}
                </ListboxOption>
              ))}
            </ListboxList>
          </ListboxPopover>
        </ListboxInput>
      </div>
      <div className="flex items-stretch flex-wrap justify-between">
        {filteredCountries.map((country) => (
          <Link key={country.cca3} href={`/${country.cca3.toLowerCase()}`}>
            <a className="w-full desktop:w-1/5 mx-px mb-16">
              <CountryCard country={country} />
            </a>
          </Link>
        ))}
      </div>
    </>
  );
};

export default Home;
