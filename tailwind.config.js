const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  theme: {
    screens: {
      mobile: '375px',
      desktop: '1440px',
    },
    extend: {
      colors: {
        lightmodeBG: 'hsl(0, 0%, 98%)',
        darkmodeBG: 'hsl(207, 26%, 17%)',
        lightmodeElement: 'hsl(0, 0%, 100%)',
        darkmodeElement: 'hsl(209, 23%, 22%)',
        lightmodeText: 'hsl(200, 15%, 8%)',
        darkmodeText: 'hsl(0, 0%, 100%)',
        lightmodeInput: 'hsl(0, 0%, 52%)',
      },
      fontFamily: {
        sans: ['Nunito Sans', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
