# Zyda Frontend Test - REST Countries API with color theme switcher

This is a solution to the [REST Countries API with color theme switcher task](https://github.com/zydalabs/FrontEnd-Test). Zyda Frontend Task helps us test your coding skills by building a realistic project.

## Table of contents

- [Zyda Frontend Test - REST Countries API with color theme switcher](#zyda-frontend-test---rest-countries-api-with-color-theme-switcher)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The task](#the-task)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
  - [Author](#author)

## Overview

### The task

Users should be able to:

- See all countries from the API on the homepage
- Search for a country using an `input` field
- Filter countries by region
- Click on a country to see more detailed information on a separate page
- Click back button to return to countries page from the details page
- Toggle the color scheme between light and dark mode

### Screenshot

![mobile-home-light](./screenshots/mobile-home-light.png)
![mobile-home-dark](./screenshots/mobile-home-dark.png)
![desktop-home-light](./screenshots/desktop-home-light.png)
![desktop-home-dark](./screenshots/desktop-home-dark.png)
![mobile-details-light](./screenshots/mobile-details-light.png)
![mobile-details-dark](./screenshots/mobile-details-dark.png)
![desktop-details-light](./screenshots/desktop-details-light.png)
![desktop-details-dark](./screenshots/desktop-details-dark.png)

### Links

- [Solution URL](https://gitlab.com/ahmedsabdelaziz/countries)
- [Live Site URL](https://countries-blush.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- Flexbox
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Next.js](https://nextjs.org/) - React framework
- [Tailwind](https://tailwindcss.com/) - For styles

### What I learned

Deploying to Vercel

### Continued development

- Data caching
- Optimistic UI
- Software architecture (especially for huge code bases)

## Author

- Linkedin - [Ahmed Abdel-Aziz]( www.linkedin.com/in/ahmad-abdel-aziz-95b40aa4)
