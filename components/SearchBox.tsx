import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

export interface SearchBoxProps {
  className?: string;
  placeholder?: string;
  value: string;
  onSearch: (search: string) => void;
}
export default function SearchBox({className, placeholder, value, onSearch}: SearchBoxProps) {
  return (
    <div
      className={`
          ${className}
          bg-lightmodeElement
          dark:bg-darkmodeElement
          flex
          rounded-md
          py-4
          px-6
          items-center
          gap-4
          focus-within:outline-[auto]
        `}
    >
      <FontAwesomeIcon className="text-lightmodeInput dark:text-darkmodeText" icon={faSearch} />
      <input
        type="text"
        className="bg-transparent focus:outline-none flex-grow dark:placeholder-darkmodeText"
        placeholder={placeholder}
        value={value}
        onChange={(e) => onSearch(e.target.value)}
      />
    </div>
  );
}
