import type {Country} from '../types/Country';

interface Props {
  country: Country;
}
export default function CountryCard({country}: Props) {
  return (
    <div
      className="
        w-full
        h-full
        flex
        flex-col
        justify-between
        rounded-md
        bg-lightmodeElement
        dark:bg-darkmodeElement
        shadow-sm
        overflow-hidden
        "
    >
      <img src={country.flags[0]} className="w-full" alt="flag" />
      <div className="px-6 pt-8 pb-12">
        <h2 className="font-bold mb-2 overflow-hidden whitespace-nowrap overflow-ellipsis">
          {country.name.common}
        </h2>
        <div className="text-sm">
          <div className="my-1">
            <span className="font-semibold">Area: </span>
            <span className="font-light">{country.area}</span>
          </div>
          <div className="my-1">
            <span className="font-semibold">Region: </span>
            <span className="font-light">{country.region}</span>
          </div>
          <div className="my-1">
            <span className="font-semibold">Capital: </span>
            <span className="font-light">{country.capital[0]}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
